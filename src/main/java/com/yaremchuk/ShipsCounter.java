package com.yaremchuk;

public class ShipsCounter {
    private static final int[][] ships = {
            {0, 0, 0, 0, 0, 0, 0, 1, 0, 0,},
            {0, 1, 0, 0, 0, 0, 0, 1, 0, 0,},
            {0, 1, 0, 1, 1, 0, 0, 0, 0, 0,},
            {0, 1, 0, 1, 1, 0, 0, 1, 1, 1,},
            {0, 1, 0, 0, 0, 0, 0, 0, 0, 0,},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            {1, 1, 1, 1, 0, 1, 0, 0, 0, 0,},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            {1, 1, 0, 0, 0, 0, 0, 1, 0, 0,},
            {1, 1, 0, 0, 0, 0, 0, 0, 0, 1,}
    };

    public static void main(String[] args) {
        System.out.println(numberOfShips() + " ships on place");
    }

    private static void markShip(int[][] ships, int x, int y) {
        if (ships[x][y] == 0 || ships[x][y] == 2) {
            return;
        }
        ships[x][y] = 2;

        if (x > 0) {
            markShip(ships, x - 1, y);
        }
        if (y > 0) {
            markShip(ships, x, y - 1);
        }
        if (y < ships[x].length - 1) {
            markShip(ships, x, y + 1);
        }
        if (x < ships.length - 1) {
            markShip(ships, x + 1, y);
        }
    }

    private static int numberOfShips() {
        int count = 0;

        for (int i = 0; i < ShipsCounter.ships.length; i++) {
            for (int j = 0; j < ShipsCounter.ships[i].length; j++) {
                if (ShipsCounter.ships[i][j] == 1) {
                    count++;
                    markShip(ShipsCounter.ships, i, j);
                    printShips();
                }
            }
        }
        return count;
    }

    private static void printShips() {
        System.out.println("Ships (2 - marked ships, 1 - unmarked ships)");
        for (int[] row : ShipsCounter.ships) {
            for (int element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}